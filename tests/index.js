import test from "ava";
import * as Color from "..";

test(`Generates valid HEXes`, (t) => {
  const color = {
    red: 10,
    green: 5,
    blue: 20,
    alpha: 80,
  };

  const correctHex = `#500a0514`;

  t.is(Color.createHex(color), correctHex);
});

test(`Generates valid CSS RGB functions`, (t) => {
  const color = {
    red: 10,
    green: 5,
    blue: 20,
    alpha: 80,
  };

  const correctCssRgb = `rgba(10, 5, 20, ${80 / 255})`;

  t.is(Color.createCssRgb(color), correctCssRgb);
});

test(`Calculates brightness correctly`, (t) => {
  const color = {
    red: 255,
    green: 255,
    blue: 255,
    alpha: 255,
  };

  t.is(Color.brightness(color), 1);
});

test(`Detects light colors correctly`, (t) => {
  const color = {
    red: 255,
    green: 200,
    blue: 0,
    alpha: 255,
  };

  t.true(Color.isLight(color));
});

test(`Detects dark colors correctly`, (t) => {
  const color = {
    red: 255,
    green: 0,
    blue: 255,
    alpha: 255,
  };

  t.false(Color.isLight(color));
});

test(`Converts RGB to HSL correctly`, (t) => {
  t.deepEqual(
    Color.rgbToHsl({ red: 255, green: 200, blue: 0, alpha: 255 }),
    { hue: 47.05882352941176, saturation: 1, lightness: 0.5, alpha: 255 },
  );

  t.deepEqual(
    Color.rgbToHsl({ red: 0, green: 0, blue: 0, alpha: 255 }),
    { hue: 0, saturation: 0, lightness: 0, alpha: 255 },
  );
});

test(`Converts HSL to RGB correctly`, (t) => {
  const hsl = {
    hue: 60,
    saturation: 0.8,
    lightness: 0.2,
    alpha: 255,
  };

  const rgb = {
    red: 92,
    green: 92,
    blue: 10,
    alpha: 255,
  };

  t.deepEqual(Color.hslToRgb(hsl), rgb);
});

test(`Converts RGB to HSB correctly`, (t) => {
  t.deepEqual(
    Color.rgbToHsb({ red: 255, green: 0, blue: 0, alpha: 0 }),
    { hue: 0, saturation: 1, brightness: 1, alpha: 0 },
  );
  t.deepEqual(
    Color.rgbToHsb({ red: 0, green: 255, blue: 0, alpha: 0 }),
    { hue: 120, saturation: 1, brightness: 1, alpha: 0 },
  );
  t.deepEqual(
    Color.rgbToHsb({ red: 0, green: 0, blue: 255, alpha: 0 }),
    { hue: 240, saturation: 1, brightness: 1, alpha: 0 },
  );
  t.deepEqual(
    Color.rgbToHsb({ red: 100, green: 150, blue: 200, alpha: 0 }),
    { hue: 210, saturation: 0.5, brightness: 0.7843137254901961, alpha: 0 },
  );
  t.deepEqual(
    Color.rgbToHsb({ red: 0, green: 0, blue: 0, alpha: 0 }),
    { hue: 0, saturation: 0, brightness: 0, alpha: 0 },
  );
  t.deepEqual(
    Color.rgbToHsb({ red: 255, green: 255, blue: 255, alpha: 0 }),
    { hue: 0, saturation: 0, brightness: 1, alpha: 0 },
  );
});

test(`Converts HSB to RGB correctly`, (t) => {
  t.deepEqual(
    { red: 255, green: 0, blue: 0, alpha: 0 },
    Color.hsbToRgb({ hue: 0, saturation: 1, brightness: 1, alpha: 0 }),
  );
  t.deepEqual(
    { red: 0, green: 255, blue: 0, alpha: 0 },
    Color.hsbToRgb({ hue: 120, saturation: 1, brightness: 1, alpha: 0 }),
  );
  t.deepEqual(
    { red: 0, green: 0, blue: 255, alpha: 0 },
    Color.hsbToRgb({ hue: 240, saturation: 1, brightness: 1, alpha: 0 }),
  );
  t.deepEqual(
    { red: 100, green: 150, blue: 200, alpha: 0 },
    Color.hsbToRgb({ hue: 210, saturation: 0.5, brightness: 0.7843137254901961, alpha: 0 }),
  );
  t.deepEqual(
    { red: 0, green: 0, blue: 0, alpha: 0 },
    Color.hsbToRgb({ hue: 0, saturation: 0, brightness: 0, alpha: 0 }),
  );
  t.deepEqual(
    { red: 255, green: 255, blue: 255, alpha: 0 },
    Color.hsbToRgb({ hue: 0, saturation: 0, brightness: 1, alpha: 0 }),
  );
});

test(`Oerlays colors correctly`, (t) => {
  const firstColor = {
    red: 191,
    green: 44,
    blue: 123,
    alpha: 255,
  };

  const secondColor = {
    red: 109,
    green: 169,
    blue: 165,
    alpha: 140,
  };

  const finalColor = {
    red: 146,
    green: 113,
    blue: 146,
    alpha: 255,
  };

  t.deepEqual(Color.overlay(firstColor, secondColor), finalColor);
});

test(`Parses HEXes correctly`, (t) => {
  t.deepEqual(Color.parseHex(`#123`), {
    red: 0x11,
    green: 0x22,
    blue: 0x33,
    alpha: 0xff,
  });

  t.deepEqual(Color.parseHex(`#4123`), {
    red: 0x11,
    green: 0x22,
    blue: 0x33,
    alpha: 0x44,
  });

  t.deepEqual(Color.parseHex(`#123456`), {
    red: 0x12,
    green: 0x34,
    blue: 0x56,
    alpha: 0xff,
  });

  t.deepEqual(Color.parseHex(`#78123456`), {
    red: 0x12,
    green: 0x34,
    blue: 0x56,
    alpha: 0x78,
  });

  t.is(Color.parseHex(`78123456`), null);
  t.is(Color.parseHex(`#`), null);
  t.is(Color.parseHex(``), null);
  t.is(Color.parseHex(`#qwerty`), null);
});
